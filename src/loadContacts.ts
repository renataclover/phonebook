import axios from 'axios';
import { IResponseBody } from '../types';

export default async function loadContacts(): Promise<IResponseBody> {

  const response = await axios('https://qa-2019.netlify.com/.netlify/functions/addressbook');

  if (response.status !== 200) {
    console.log(`Статус запроса: ${response.status}`);
  }
  console.log(response.data);
  return response.data;
}
