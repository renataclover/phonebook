import * as express from 'express';
import findAvailable from './src/findAvailable';
import loadContacts from './src/loadContacts';

const app = express();

app.get('/', async (_, res) => {

  try {
    const result = await findAvailable(loadContacts);

    if (!result) {
      return res.status(404).send('Not found');
    }
    console.log(result);
    res.send(result);

  } catch (err) {
    console.log(err);
    res.status(500).send(err);
  }
});

app.listen(3000, () => {
  console.log('Example app listening on port 3000!');
});
