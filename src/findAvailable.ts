import { IABRecord, IResponseBody } from '../types';

export default async function findAvailable(loadData: () => Promise<IResponseBody>): Promise<any> {
  const data = await loadData();

  return Object.values(data)
    .filter((record: IABRecord) => record.enabled)
    .map((record: IABRecord) => {
      return {
        name: record.name,
        phone: record.phone,
        company: record.company,
      };
    });
}
