export declare interface IABRecord {
  name: string;
  phone: string;
  company: string;
  enabled: boolean;
}

export declare interface IResponseBody {
  [id: string]: IABRecord
}