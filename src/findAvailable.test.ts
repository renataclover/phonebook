import { IResponseBody } from '../types';
import findAvailable from './findAvailable';

describe('getPhonesList', () => {
  it('should return an array with available contacts', async () => {
    const resp: IResponseBody = {
      '0': {
        name: 'Test Name 1',
        company: 'Test Company 1',
        phone: '123-901',
        enabled: false,
      },
      '1': {
        name: 'Test Name 2',
        company: 'Test Company 2',
        phone: '123-902',
        enabled: true,
      },
    };
    const expected = [{
      company: 'Test Company 2',
      name: 'Test Name 2',
      phone: '123-902',
    }];
    const asyncMock = jest.fn().mockResolvedValue(resp);
    const actual = await findAvailable(asyncMock);

    expect(actual).toEqual(expected);
  });
});
